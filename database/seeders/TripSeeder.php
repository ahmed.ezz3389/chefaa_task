<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
            'name' => "trip1",
            'code' => "13123",
            'bus_id' => 1,
            'starting_station_id' => 1,
            'end_station_id' => 7,
            'start_date' => '2021-02-03 19:02:04',
            'end_date' => '2021-02-02 21:02:04',
        ]);

        DB::table('trips')->insert([
            'name' => "trip2",
            'code' => "1312123",
            'bus_id' => 2,
            'starting_station_id' => 3,
            'end_station_id' => 6,
            'start_date' => '2021-02-05 19:02:04',
            'end_date' => '2021-02-05 21:02:04',
        ]);

        DB::table('trips')->insert([
            'name' => "trip3",
            'code' => "1313123",
            'bus_id' => 3,
            'starting_station_id' => 2,
            'end_station_id' => 4,
            'start_date' => '2021-02-05 19:02:04',
            'end_date' => '2021-02-05 21:02:04',
        ]);
    }
}
