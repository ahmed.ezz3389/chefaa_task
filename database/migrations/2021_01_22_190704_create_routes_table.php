<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Database\Seeders\RoutesSeeder;
class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('trip_id')->nullable()->unsigned();
            $table->foreign('trip_id')->references('id')->on('trips')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('station_id')->nullable()->unsigned();
            $table->foreign('station_id')->references('id')->on('stations')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->integer('sort')->default(1)->unsigned();
            $table->timestamps();
        });

        $seeder = new RoutesSeeder();
        $seeder->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
