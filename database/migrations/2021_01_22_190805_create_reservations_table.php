<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('trip_id')->nullable()->unsigned();
            $table->foreign('trip_id')->references('id')->on('trips')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('from_route_id')->nullable()->unsigned();
            $table->foreign('from_route_id')->references('id')->on('routes')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('to_route_id')->nullable()->unsigned();
            $table->foreign('to_route_id')->references('id')->on('routes')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('seat_id')->nullable()->unsigned();
            $table->foreign('seat_id')->references('id')->on('seats')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
