<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id' => 'required|numeric|exists:trips,id',
            'from_route_id' => 'required|numeric|exists:routes,id',
            'to_route_id' => 'required|numeric|exists:routes,id',
            'seat_id' => 'required|numeric|exists:seats,id',
        ];
    }
}
