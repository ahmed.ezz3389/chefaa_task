<?php
namespace App\Repositories;

use App\Models\trip;
use App\Models\route;
use Illuminate\Database\Eloquent\Builder;
use App\Models\reservation;
use App\Interfaces\ReservationRepositoryInterface;
use Auth;
class ReservationRepository implements ReservationRepositoryInterface
{

    public function reserve_trip_seat($request)
    {
        $from_station_number = route::whereId($request['from_route_id'])->value('sort');
        $to_station_number = route::whereId($request['to_route_id'])->value('sort');
        $reserved_seat = reservation::where('trip_id', $request['trip_id'])->where('seat_id', $request['seat_id'])->whereHas('from_route', function (Builder $query) use ($from_station_number, $to_station_number)
        {
            $query->whereBetween('sort', [$from_station_number, $to_station_number -1]);
        })->orWhereHas('to_route', function (Builder $query) use ($from_station_number, $to_station_number)
        {
            $query->whereBetween('sort', [$from_station_number +1, $to_station_number]);
        })->first();
        if ($reserved_seat)
        {
            return response()->json(['succes' => false, 'errors' => ['reservation ' => ['this seat is reserved before']]], 400);
        }
        $request['user_id'] = Auth::id();
        reservation::create($request->only('trip_id', 'from_route_id', 'to_route_id', 'seat_id', 'user_id'));
        return Response()
            ->json(['message' => 'success']);
    }
}

