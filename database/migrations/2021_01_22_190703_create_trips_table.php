<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Database\Seeders\TripSeeder;
class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->bigInteger('bus_id')->nullable()->unsigned();
            $table->foreign('bus_id')->references('id')->on('buses')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('starting_station_id')->nullable()->unsigned();
            $table->foreign('starting_station_id')->references('id')->on('stations')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->bigInteger('end_station_id')->nullable()->unsigned();
            $table->foreign('end_station_id')->references('id')->on('stations')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->dateTime('start_date');
            $table->dateTime('end_date');


            $table->timestamps();
        });
        $seeder = new TripSeeder();
        $seeder->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
