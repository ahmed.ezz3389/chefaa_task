<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reservation extends Model
{
    use HasFactory;

    protected $guarded = [];
    public function from_route()
    {
        return $this->belongsTo(route::class,'from_route_id','id');
    }

    public function to_route()
    {
        return $this->belongsTo(route::class,'to_route_id','id');
    }
}
