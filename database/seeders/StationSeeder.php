<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stations')->insert([
            'name' => "cairo",
        ]);

        DB::table('stations')->insert([
            'name' => "banha",
        ]);

        DB::table('stations')->insert([
            'name' => "quesna",
        ]);

        DB::table('stations')->insert([
            'name' => "sab3",
        ]);

        DB::table('stations')->insert([
            'name' => "kafr el ziat",
        ]);

        DB::table('stations')->insert([
            'name' => "damanhor",
        ]);

        DB::table('stations')->insert([
            'name' => "alex",
        ]);
    }
}
