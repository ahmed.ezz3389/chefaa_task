<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB,Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "test1",
            'email' => 'test1@chefaa.com',
            'password' => Hash::make("secret"),
        ]);

        DB::table('users')->insert([
            'name' => "test2",
            'email' => 'test2@chefaa.com',
            'password' => Hash::make("secret"),
        ]);

        DB::table('users')->insert([
            'name' => "test3",
            'email' => 'test3@chefaa.com',
            'password' => Hash::make("secret"),
        ]);

        DB::table('users')->insert([
            'name' => "test4",
            'email' => 'test4@chefaa.com',
            'password' => Hash::make("secret"),
        ]);
    }
}
