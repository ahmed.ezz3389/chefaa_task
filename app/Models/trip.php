<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilter;

class trip extends Model
{
    use HasFactory;

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query) ;
    }


    public function routes()
    {
        return $this->hasMany(route::class);
    }

    public function start_station()
    {
        return $this->belongsTo(station::class,'starting_station_id','id');
    }

    public function end_station()
    {
        return $this->belongsTo(station::class,'end_station_id','id');
    }
}
