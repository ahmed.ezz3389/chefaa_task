<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class BusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buses')->insert([
            'name' => "bus1",
            'code' => "13123",
            'number_of_seats' => 10,
        ]);

        DB::table('buses')->insert([
            'name' => "bus2",
            'code' => "1312123",
            'number_of_seats' => 10,
        ]);

        DB::table('buses')->insert([
            'name' => "bus3",
            'code' => "1312o093",
            'number_of_seats' => 10,
        ]);

    }
}
