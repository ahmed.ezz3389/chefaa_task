<?php
namespace App\Filters;
use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use App\Models\route;
class tripFilter extends QueryFilter{

     public function date($filter){
          return $this->builder->whereDate('start_date',$filter);
     }

     public function bus($filter){
        return $this->builder->where('bus_id',$filter);
     }

     public function name($filter){
        return $this->builder->where('name','like','%'.$filter.'%');
     }

   public function code($filter){
    return $this->builder->where('code','like','%'.$filter.'%');
     }


     public function routes($filter){
        $from_station_number = route::where('station_id',$filter['from_station'])->value('sort');
        $to_station_number = route::where('station_id',$filter['to_station'])->value('sort');
        $from_station = $filter['from_station'];
        $to_station = $filter['to_station'];
        return $this->builder->WhereHas('routes', function (Builder $query) use($from_station,$to_station) {
            $query->where('station_id', $from_station);
        })->WhereHas('routes', function (Builder $query) use($from_station,$to_station) {
            $query->Where('station_id', $to_station);
        });
     }

    public function sortingModel($filter){
        if($filter['sortingExpression'] == 'created'){
             return $this->builder->orderBy('id','desc');
         }
        if($filter['sortingDirection'] == 1){
             $filter['sortingDirection']= 'desc';
        }else{
             $filter['sortingDirection']= 'asc';
        }
        return $this->builder->orderBy($filter['sortingExpression'],$filter['sortingDirection']);
   }
}
