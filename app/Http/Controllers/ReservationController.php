<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use App\Models\reservation;
use App\Models\route;
use Illuminate\Http\Request;
use App\Http\Requests\ReservationRequest;
use App\Interfaces\ReservationRepositoryInterface;
use Auth;
class ReservationController extends Controller
{
    public function __construct(ReservationRepositoryInterface $reservation_repository){
        $this->reservation_repository = $reservation_repository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservationRequest $request)
    {
        $reservations =$this->reservation_repository->reserve_trip_seat($request);

        return $reservations;
    }

}
