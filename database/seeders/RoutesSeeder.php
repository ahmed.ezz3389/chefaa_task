<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 1,
            'sort' => 1,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 2,
            'sort' => 2,
        ]);


        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 3,
            'sort' => 3,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 4,
            'sort' => 4,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 5,
            'sort' => 5,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 6,
            'sort' => 6,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 1,
            'station_id' => 7,
            'sort' => 7,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 2,
            'station_id' => 3,
            'sort' => 1,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 2,
            'station_id' => 4,
            'sort' => 2,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 2,
            'station_id' => 5,
            'sort' => 3,
        ]);

        DB::table('routes')->insert([
            'trip_id' => 2,
            'station_id' => 6,
            'sort' => 4,
        ]);
    }
}
