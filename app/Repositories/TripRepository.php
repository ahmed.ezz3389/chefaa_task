<?php
namespace App\Repositories;

use App\Models\trip;
use App\Models\route;
use App\Interfaces\TripRepositoryInterface;

class TripRepository implements TripRepositoryInterface
{

    public function filter_trips($request, $filter)
    {
        $trips = trip::filter($filter)->with('start_station:id,name', 'end_station:id,name')
            ->paginate(10);

        $paginator = tap($trips, function ($paginatedInstance) use ($request)
        {
            return $paginatedInstance->getCollection()->transform(function ($value) use ($request)
            {
                if (isset($request['routes']))
                {
                    $from_station_number = route::where('trip_id', $value->id)
                        ->where('station_id', $request['routes']['from_station'])->value('sort');
                    $to_station_number = route::where('trip_id', $value->id)
                        ->where('station_id', $request['routes']['to_station'])->value('sort');
                    $routes = route::where('trip_id', $value->id)
                        ->whereBetween('sort', [$from_station_number, $to_station_number])->with('station:id,name')
                        ->orderBy('sort', 'asc')
                        ->get();
                }
                else
                {
                    $routes = route::where('trip_id', $value->id)
                        ->with('station:id,name')
                        ->orderBy('sort', 'asc')
                        ->get();
                }
                $routes = collect($routes)->map(function ($item)
                {
                    return ['id' => $item->id, 'station' => $item
                        ->station->name, 'sort' => $item->sort];
                });

                return ['id' => $value->id, 'name' => $value->name, 'code' => $value->code, 'start_date' => $value->start_date, 'end_date' => $value->end_date, 'start_station' => $value->start_station, 'end_station' => $value->end_station, 'routes' => $routes];
            });
        });

        return $paginator;
    }

}

