<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Interfaces\TripRepositoryInterface',
            'App\Repositories\TripRepository');

            $this->app->bind(
                'App\Interfaces\ReservationRepositoryInterface',
                'App\Repositories\ReservationRepository');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
